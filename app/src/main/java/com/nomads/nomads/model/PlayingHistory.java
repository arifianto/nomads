package com.nomads.nomads.model;

import com.google.gson.annotations.SerializedName;

public class PlayingHistory{

	@SerializedName("start_time")
	private String startTime;

	@SerializedName("distance")
	private double distance;

	@SerializedName("end_time")
	private String endTime;

	@SerializedName("singleLineLocation")
	private String singleLineLocation;

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public void setDistance(double distance){
		this.distance = distance;
	}

	public double getDistance(){
		return distance;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	public void setSingleLineLocation(String singleLineLocation){
		this.singleLineLocation = singleLineLocation;
	}

	public String getSingleLineLocation(){
		return singleLineLocation;
	}

	@Override
 	public String toString(){
		return 
			"PlayingHistory{" + 
			"start_time = '" + startTime + '\'' + 
			",distance = '" + distance + '\'' + 
			",end_time = '" + endTime + '\'' + 
			",singleLineLocation = '" + singleLineLocation + '\'' + 
			"}";
		}
}