package com.nomads.nomads.model;

public class VideoModel {

    private int campaignId;
    private int id;
    private String path;
    private String name;
    private String slug;
    private long expiredDate;
    private String status;

    public VideoModel() {
    }

    public VideoModel(int campaignId, int id, String path, String name, String slug, long expiredDate, String status) {
        this.campaignId = campaignId;
        this.id = id;
        this.path = path;
        this.name = name;
        this.slug = slug;
        this.expiredDate = expiredDate;
        this.status = status;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public long getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(long expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
