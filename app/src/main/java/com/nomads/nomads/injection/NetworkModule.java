package com.nomads.nomads.injection;

import android.app.Application;
import android.util.Log;

import com.nomads.nomads.BuildConfig;
import com.nomads.nomads.network.service.ApiService;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private final Application app;

    public NetworkModule(Application app) {
        this.app = app;
    }

    @Provides
    @Singleton
    @Named("service_name")
    Retrofit provideRetrofit() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.DEV_API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        Log.i("NetworkModule", retrofit.baseUrl().toString());
        return retrofit;
    }

    @Provides
    @Singleton
    ApiService provideApiService() {
        return provideRetrofit().create(ApiService.class);
    }
}
