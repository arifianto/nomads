package com.nomads.nomads.injection;

import android.app.Application;

import com.nomads.nomads.controller.NetworkController;
import com.nomads.nomads.controller.SettingsController;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ControllerModule {

    private Application application;

    public ControllerModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    SettingsController providesSystemController(Application application) {
        return new SettingsController(application.getApplicationContext());
    }

    @Provides
    @Singleton
    NetworkController providesNetworkController(SettingsController settingsController) {
        return new NetworkController(settingsController);
    }
}