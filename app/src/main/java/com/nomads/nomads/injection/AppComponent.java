package com.nomads.nomads.injection;

import com.nomads.nomads.MainActivity;
import com.nomads.nomads.MainApplication;
import com.nomads.nomads.SplashScreen;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        dependencies = {},
        modules = {AppModule.class, RoomModule.class, NetworkModule.class, ControllerModule.class}
)
public interface AppComponent {

    // MainApplication
    void inject(MainApplication application);

    // Activities
    void inject(MainActivity activity);
    void inject(SplashScreen splashScreen);

}
