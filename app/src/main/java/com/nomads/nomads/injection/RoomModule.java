package com.nomads.nomads.injection;

import android.app.Application;

import androidx.room.Room;

import com.nomads.nomads.orm.AppDatabase;
import com.nomads.nomads.orm.dao.CampaignDao;
import com.nomads.nomads.orm.dao.UserDao;
import com.nomads.nomads.orm.dao.VideoDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {
    private AppDatabase mDatabase;

    public RoomModule(Application application) {
        synchronized (AppDatabase.class) {
            if (mDatabase == null) {
                mDatabase = Room.databaseBuilder(
                        application,
                        AppDatabase.class, "nomads_db")
                        .allowMainThreadQueries()
                        .build();
            }
        }
    }

    @Provides
    @Singleton
    AppDatabase providesDatabase() {
        return mDatabase;
    }

    // ======================== DAO ========================

    @Provides
    @Singleton
    VideoDao provideVideoDao(AppDatabase db) {
        return db.videoDao();
    }

    @Provides
    @Singleton
    UserDao provideUserDao(AppDatabase db) {
        return db.userDao();
    }

    @Provides
    @Singleton
    CampaignDao provideCampaignDao(AppDatabase db){
        return db.campaignDao();
    }
}
