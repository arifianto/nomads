package com.nomads.nomads.injection;


import com.nomads.nomads.viewmodel.UserViewModel;
import com.nomads.nomads.viewmodel.VideoViewModel;

import dagger.Subcomponent;

@Subcomponent
public interface ViewModelSubComponent {

    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    // Add View Model here
    VideoViewModel videoViewModel();
    UserViewModel userViewModel();
}
