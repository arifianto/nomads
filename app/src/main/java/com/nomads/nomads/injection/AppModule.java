package com.nomads.nomads.injection;


import android.app.Application;

import com.nomads.nomads.network.service.ApiService;
import com.nomads.nomads.orm.dao.CampaignDao;
import com.nomads.nomads.orm.dao.UserDao;
import com.nomads.nomads.orm.dao.VideoDao;
import com.nomads.nomads.repository.CampaignRepository;
import com.nomads.nomads.repository.UserRepository;
import com.nomads.nomads.repository.VideoRepository;
import com.nomads.nomads.viewmodel.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(subcomponents = {ViewModelSubComponent.class})
public class AppModule {
    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    ViewModelFactory provideViewModelFactory(ViewModelSubComponent.Builder viewModelComponent) {
        return new ViewModelFactory(viewModelComponent.build());
    }

    @Provides
    @Singleton
    VideoRepository provideVideoRepository(VideoDao dao, UserDao userDao, ApiService apiservice) {
        return new VideoRepository(dao, userDao, apiservice);
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository(UserDao dao, ApiService apiservice) {
        return new UserRepository(dao, apiservice);
    }

    @Provides
    @Singleton
    CampaignRepository provideCampaignRepository(CampaignDao dao, ApiService apiService){
        return new CampaignRepository(dao, apiService);
    }


}
