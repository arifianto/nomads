package com.nomads.nomads.autoStartUp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.nomads.nomads.MainActivity;
import com.nomads.nomads.MainApplication;
import com.nomads.nomads.SplashScreen;
import com.nomads.nomads.autoUpdateApk.UpdateChecker;

public class NomadsReceiver extends BroadcastReceiver {
    private static final String TAG = "Alarm Receiver";
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        Log.e(TAG, "onReceive: Receiver");
        String action = intent.getAction();

        if (action != null) {
            if (action.equals("android.intent.action.BOOT_COMPLETED")) {
                // CODE
                new Handler().postDelayed(() -> {
                    Log.e(TAG, "onReceive: Action boot up");
                    Intent i = new Intent(context, SplashScreen.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }, 4000);
            } else if (action.equals("com.nomads.nomads.alarmManager")) {
                // CODE
                Log.e(TAG, "onReceive: Action alarm");
                Intent i = new Intent(context, SplashScreen.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }

    }

}