package com.nomads.nomads;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.downloader.Error;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.nomads.nomads.autoStartUp.NomadsReceiver;
import com.nomads.nomads.autoUpdateApk.Constants;
import com.nomads.nomads.autoUpdateApk.DownloadService;
import com.nomads.nomads.controller.NetworkController;
import com.nomads.nomads.controller.SettingsController;
import com.nomads.nomads.downloadVideo.DownloadVideoListener;
import com.nomads.nomads.helper.PermissionHelper;
import com.nomads.nomads.helper.Utils;
import com.nomads.nomads.model.VideoModel;
import com.nomads.nomads.network.response.updateApp.Data;
import com.nomads.nomads.network.response.updateApp.UpdateAppResponse;
import com.nomads.nomads.network.response.video.VideoResponse;
import com.nomads.nomads.network.response.video.historydownloadvideo.HistoryDownloadVideo;
import com.nomads.nomads.orm.model.EntityHistoryDownload;
import com.nomads.nomads.viewmodel.VideoViewModel;
import com.nomads.nomads.viewmodel.ViewModelFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.reactivestreams.Subscription;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.FlowableSubscriber;
import io.reactivex.schedulers.Schedulers;

public class SplashScreen extends AppCompatActivity implements
        MediaPlayer.OnCompletionListener,
        ConnectivityReceiver.ConnectivityReceiverListener,
        DownloadVideoListener {

    // set dependency injection
    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    NetworkController networkController;

    private ConstraintLayout cl;
    private ConstraintLayout noVideo;
    private VideoViewModel videoViewModel;
    private List<Subscription> sub = new ArrayList<>();
    private ProgressBar progress_bar;
    private TextView tv_progress;
    private LinearLayout layoutStatus;
    private String TAG = getClass().getCanonicalName();
    private String path = null;
    private final List<String> urlDownload = new ArrayList<String>();
    private final List<String> nameDownload = new ArrayList<String>();
    private final List<Long> expVideo = new ArrayList<Long>();
    private final List<Integer> videoId = new ArrayList<Integer>();
    private long bandwidthUsage = 0;
    private String totalBanwidthUsage, showDownloadName;
    private TextView downloadVideoName, versionName;
    private int indexOfDownload = 0, timeOutRetryGetVideo = 0;
    private int downloadId = 0;
    private VideoView videoView = null;
    private int videoSquence = 0;
    private List<String> playlist = new ArrayList<>();
    private int countTimeoutDownload = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainApplication app = (MainApplication) getApplication();
        app.getComponent().inject(this);
        videoViewModel = new ViewModelProvider(this, viewModelFactory).get(VideoViewModel.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        showNavigationBar(false);
        initComponent();
        triggerAlarm();
    }

    private void initComponent() {
        // initiate downloader view
        progress_bar = findViewById(R.id.progress_bar);
        downloadVideoName = findViewById(R.id.dname);
        tv_progress = findViewById(R.id.tv_progress);
        layoutStatus = findViewById(R.id.layout_status);
        // initiate UI view
        cl = findViewById(R.id.constriant_layout_splash);
        versionName = findViewById(R.id.version);
        noVideo = findViewById(R.id.noVideo);

        videoView = findViewById(R.id.vv_splash);

        initialVariable();
        initVideo();
    }

    private void initVideo() {
        videoView.setOnCompletionListener(this);
        playlist.add("android.resource://" + getPackageName() + "/" + R.raw.v1);
        playlist.add("android.resource://" + getPackageName() + "/" + R.raw.v2);

        videoView.setVideoURI(Uri.parse(playlist.get(videoSquence)));
        videoView.start();
        videoSquence++;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        videoView.setVideoURI(Uri.parse(playlist.get(videoSquence)));
        videoView.start();
        videoSquence++;
        if (videoSquence > playlist.size() - 1) {
            videoSquence = 0;
        }
    }

    private void initialVariable() {
        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES) + "";
        versionName.setText("Version " + BuildConfig.VERSION_NAME);
        noVideo.setVisibility(View.GONE);
        checkUpdateApp();
    }

    // 1. Cek Update
    private void checkUpdateApp() {
        layoutStatus.setVisibility(View.VISIBLE);
        downloadVideoName.setText("Check Update App");
        videoViewModel.getUpdateApp()
                .subscribeOn(Schedulers.io())
                .subscribe(new FlowableSubscriber<UpdateAppResponse>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        sub.add(s);
                        s.request(Long.MAX_VALUE);
                        // show loading
                    }

                    @Override
                    public void onNext(UpdateAppResponse updateAppResponse) {
                        //stop loading
                        try {
                            Gson gson = new Gson();
                            String data = gson.toJson(updateAppResponse.getData());
                            JSONObject json = new JSONObject(data);
                            String apkUrl = json.getString("url");
                            int apkCode = json.getInt("version_code");
                            int versionCode = Utils.getVersionCode(SplashScreen.this);
                            if (apkCode > versionCode) {
                                Log.e("updating 1", "updating 1");
                                goToDownload(SplashScreen.this, apkUrl);
                                layoutStatus.setVisibility(View.VISIBLE);
                                downloadVideoName.setText("Updating App");
                                progress_bar.setIndeterminate(true);
                                progress_bar.getIndeterminateDrawable().setColorFilter(
                                        Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN
                                );
                            } else {
                                // update UI
                                checkConnection();
                                downloadVideoName.setText("App Updated");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            playWithoutInternet();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        //stop loading
                        checkConnection();
                        Log.e(TAG, "onError: " + t.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void goToDownload(Context context, String downloadUrl) {
        Intent intent = new Intent(context.getApplicationContext(), DownloadService.class);
        intent.putExtra(Constants.APK_DOWNLOAD_URL, downloadUrl);
        context.startService(intent);
    }

    // 2. Cek Connection
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            // request permission
            PermissionHelper.getInstance(this).permissionListener(() -> new Handler().postDelayed(() -> {
                cHeckExpiredVideo();
            }, 300));
            PermissionHelper.getInstance(this).checkAndRequestPermissions();
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar.make(cl, message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
            playWithoutInternet();
        }
    }

    private void triggerAlarm() {
        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent receiverIntent = new Intent(getApplicationContext(), NomadsReceiver.class);
        receiverIntent.setAction("com.nomads.nomads.alarmManager");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 12, receiverIntent, PendingIntent.FLAG_UPDATE_CURRENT); //The second parameter is unique to this PendingIntent,
        // set hours 24 hour format
        int hour = 23;
        int minute = 10;
        Calendar alarmCalendarTime = Calendar.getInstance(); //Convert to a Calendar instance to be able to get the time in milliseconds to trigger the alarm
        alarmCalendarTime.set(Calendar.HOUR_OF_DAY, hour);
        alarmCalendarTime.set(Calendar.MINUTE, minute);
        alarmCalendarTime.set(Calendar.SECOND, 00); //Must be set to 0 to start the alarm right when the minute hits 30
        // set repeat
        if (alarmMgr != null) {
            Log.e(TAG, "triggerAlarm: available");
            alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                    alarmCalendarTime.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY,
                    alarmIntent);
        } else {
            Log.e(TAG, "triggerAlarm: null");
        }
    }

    private void playWithoutInternet() {
        Log.e(TAG, "playWithoutInternet: play without internet");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                syncVideo();
            }
        }, 10000);
    }

    // 3. Cek Expired if expired do video delete
    private void cHeckExpiredVideo() {
        List<String> videoExpired = videoViewModel.getExpiredVideo(Utils.parseTimetoLong(Utils.getTime()));
        String deletePath = path + "/";
        if (videoExpired != null) {
            for (int i = 0; i < videoExpired.size(); i++) {
                File file = new File(deletePath + videoExpired.get(i) + ".mp4");
                if (file.exists()) {
                    String deleteCmd = "rm -r " + file;
                    Runtime runtime = Runtime.getRuntime();
                    try {
                        runtime.exec(deleteCmd);
                        videoViewModel.deleteFieldHistoryDownload(videoExpired.get(i));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        Log.e(TAG, "cHeckExpiredVideo: cek exipred Success");
        getVideo();
    }

    // 4. Get Playlist Video and insert to local db
    private void getVideo() {
        Log.e(TAG, "onNext: getting video");
        Log.e(TAG, "getVideo: IMEI " + Utils.getIMEI(this));
        videoViewModel.getVideoesUsingRx(Utils.getIMEI(this))
                .subscribeOn(Schedulers.io())
                .subscribe(new FlowableSubscriber<VideoResponse>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        sub.add(s);
                        s.request(Long.MAX_VALUE);
                        // show loading
                    }

                    @Override
                    public void onNext(VideoResponse videoResponse) {
                        new Handler().postDelayed(() -> {
                            Log.e(TAG, "onNext: get video success");
                            syncVideo();
                        }, 3000);
                    }

                    @Override
                    public void onError(Throwable t) {
                        //stop loading
                        Log.e(TAG, "onNext: GET PLAYLIST error");
                        timeOutRetryGetVideo++;
                        if (timeOutRetryGetVideo > 1) {
                            // sync video with connection problem
                            Log.e(TAG, "onError: sync video with connection problem");
                            syncVideo();
                        } else {
                            getVideo();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    // 5. Sync Video playlist and download if not exist
    private void syncVideo() {
        List<String> videoName = videoViewModel.getVideoAllbyName();
        Log.e(TAG, "syncVideo SHYNC: " + videoName);
        for (int i = 0; i < videoName.size(); i++) {
            /*if (fileExist(videoName.get(i))) {
                Log.e(TAG, "syncVideo: file exist");
            } else {*/
            List<VideoModel> strUrl = videoViewModel.getVideoAllbySlug(videoName.get(i));
            Log.e(TAG, "==================" + strUrl);
            // save to tmp list
            videoId.add(strUrl.get(0).getId());
            nameDownload.add(videoName.get(i));
            urlDownload.add(strUrl.get(0).getPath());
            expVideo.add(strUrl.get(0).getExpiredDate());
            Log.e(TAG, "==================" + videoName.get(i) + " URL " + strUrl.get(0).getPath());

            //            }
        }
        //go download video
        if (!nameDownload.isEmpty()) {
            // delete previous video
            deleteVideoPlaylist();
            Log.e(TAG, "syncVideo: delete success");
            Log.e(TAG, "syncVideo: Sync success");
            downloadVideo(urlDownload.get(indexOfDownload), nameDownload.get(indexOfDownload), expVideo.get(indexOfDownload));
        } else {
            Log.e(TAG, "syncVideo: Sync success");
            if (videoName.size() > 0) {
                goPlay();
            } else {
                showNoVideo();
            }
        }
    }

    private void deleteVideoPlaylist() {
        String deletePath = path + "/";
        String deleteCmd = "rm -r " + deletePath;
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec(deleteCmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "cHeckExpiredVideo: cek exipred Success");
    }

    public boolean fileExist(String fileName) {
        String checkStr = path + "/" + fileName + ".mp4";
        File file = new File(checkStr);
        return file.exists();
    }

    private void showNoVideo() {
        noVideo.setVisibility(View.VISIBLE);
    }

    // download video
    private void downloadVideo(String url, String fileName, long expDate) {
        downloadVideoName.setText("Download Video");
        layoutStatus.setVisibility(View.VISIBLE);
        progress_bar.setIndeterminate(true);
        progress_bar.getIndeterminateDrawable().setColorFilter(
                Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN
        );
        downloadId = Utils.downloadVideo(url, fileName, path, this);
        showDownloadName = fileName;
        Log.e(TAG, "downloadVideo: id " + downloadId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");
        // register connection status listener
        if (indexOfDownload == 0) {

        } else {
            PRDownloader.resume(downloadId);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    // callback permission request
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelper.getInstance(this).onRequestCallBack(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause: ");
        PRDownloader.pause(downloadId);
    }

    // download video listener
    @Override
    public void onDownloadProgress(Progress progress) {
//        Log.e(TAG, "onDownloadProgress: ");
        long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
        bandwidthUsage = bandwidthUsage + progress.currentBytes;
        totalBanwidthUsage = SettingsController.getProgressDisplayLine(bandwidthUsage);
        progress_bar.setProgress((int) progressPercent);
        downloadVideoName.setText("Download Video : " + showDownloadName);
        tv_progress.setText(SettingsController.getProgressDisplayLine(progress.currentBytes, progress.totalBytes));
        progress_bar.setIndeterminate(false);
    }

    @Override
    public void onDownloadComplete() {
        // Emei, idVideo, version
        if (indexOfDownload == nameDownload.size() - 1) {
            layoutStatus.setVisibility(View.GONE);
            Toast.makeText(this, "Total usage " + totalBanwidthUsage, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "totalBanwidthUsage: " + totalBanwidthUsage);
            saveToHistoryDownload(nameDownload.get(indexOfDownload), expVideo.get(indexOfDownload));
            postHistoryDownloadToServer(Utils.getIMEI(SplashScreen.this), videoId.get(indexOfDownload));
            goPlay();
        } else {
            Log.e(TAG, "onDownloadComplete: " + indexOfDownload);
            saveToHistoryDownload(nameDownload.get(indexOfDownload), expVideo.get(indexOfDownload));
            postHistoryDownloadToServer(Utils.getIMEI(SplashScreen.this), videoId.get(indexOfDownload));
            indexOfDownload++;
            downloadVideo(urlDownload.get(indexOfDownload), nameDownload.get(indexOfDownload), expVideo.get(indexOfDownload));
            Log.e(TAG, "after complete: " + indexOfDownload);
        }
    }

    @Override
    public void onDownloadError(Error error) {
        // retry download
        countTimeoutDownload++;
        downloadVideo(urlDownload.get(indexOfDownload), nameDownload.get(indexOfDownload), expVideo.get(indexOfDownload));
        if (countTimeoutDownload > 20) {
            countTimeoutDownload = 0;
            if (indexOfDownload > 1) {
                goPlay();
            } else {
                playstaticVideo();
            }
        }
    }

    private void playstaticVideo() {
        versionName.setVisibility(View.INVISIBLE);
        layoutStatus.setVisibility(View.INVISIBLE);
    }

    private void postHistoryDownloadToServer(String imei, int videoId) {
        videoViewModel.postHistoryDownload(imei, videoId)
                .subscribeOn(Schedulers.io())
                .subscribe(new FlowableSubscriber<HistoryDownloadVideo>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        sub.add(s);
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(HistoryDownloadVideo historyDownloadVideo) {
                        Log.e(TAG, "onNext: Success send history");
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e(TAG, "onError: errorr send history");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void goPlay() {
        timeOutRetryGetVideo = 0;
        PRDownloader.cleanUp(1);
        startActivity(new Intent(SplashScreen.this, MainActivity.class));
        finish();
    }

    private void saveToHistoryDownload(String videoName, long videoExpired) {
        EntityHistoryDownload entityHistoryDownload = new EntityHistoryDownload();
        entityHistoryDownload.setVideoName(videoName);
        entityHistoryDownload.setExpiredVideo(videoExpired);
        videoViewModel.insertHistoryDownload(entityHistoryDownload);
    }

    // disable status bar
    private void showNavigationBar(boolean show) {
        // This work only for android 4.4+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final View decorView = getWindow().getDecorView();
            if (decorView != null) {
                if (show) {
                    // set navigation bar status, remember to disable "setNavigationBarTintEnabled"
                    final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                    decorView.setSystemUiVisibility(flags);

                    // Code below is to handle presses of Volume up or Volume down.
                    // Without this, after pressing volume buttons, the navigation bar will
                    // show up and won't hide
                    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
                } else {
                    decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        }
    }

}
