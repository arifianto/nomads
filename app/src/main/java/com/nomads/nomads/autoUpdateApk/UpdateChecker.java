package com.nomads.nomads.autoUpdateApk;

import android.content.Context;
import android.util.Log;

public class UpdateChecker {

    public static void checkForDialog(Context context, UpdateCheckerListener listener) {
        if (context != null) {
            new CheckUpdateTask(context, Constants.TYPE_DIALOG, true);
        } else {
            Log.e(Constants.TAG, "The arg context is null");
        }
    }


    public interface UpdateCheckerListener {
        void onUpdateProgress();

        void onSuccessUpdate();
    }
}
