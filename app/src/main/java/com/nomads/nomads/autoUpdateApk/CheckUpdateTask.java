package com.nomads.nomads.autoUpdateApk;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class CheckUpdateTask extends AsyncTask<Void, Void, String> {
//    private final UpdateChecker.UpdateCheckerListener listener;
    private ProgressDialog dialog;
    private Context mContext;
    private int mType;
    private boolean mShowProgressDialog;
    private static final String url = Constants.UPDATE_URL;

    public CheckUpdateTask(Context context, int type, boolean showProgressDialog) {
//        this.listener = listener;
        this.mContext = context;
        this.mType = type;
        this.mShowProgressDialog = showProgressDialog;

    }

    protected void onPreExecute() {
        if (mShowProgressDialog) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("App need update");
            dialog.show();
        }
    }

    @Override
    protected String doInBackground(Void... voids) {
//        listener.onUpdateProgress();
        return HttpUtils.get(url);
    }

    @Override
    protected void onPostExecute(String result) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        if (!TextUtils.isEmpty(result)) {
            parseJson(result);
        }
//        listener.onSuccessUpdate();
    }

    private void parseJson(String result) {
        /*try {

            JSONObject obj = new JSONObject(result);
            JSONObject data = obj.getJSONObject(Constants.DATA);
            String updateMessage = data.getString(Constants.APK_UPDATE_CONTENT);
            String apkUrl = data.getString(Constants.APK_DOWNLOAD_URL);
            int apkCode = data.getInt(Constants.APK_VERSION_CODE);

            int versionCode = AppUtils.getVersionCode(mContext);

            if (apkCode > versionCode) {
                Log.e("updating 1", "updating 1");
                goToDownload(mContext, apkUrl);
            } else if (mShowProgressDialog) {
                Toast.makeText(mContext, "app uptodate", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            Log.e(Constants.TAG, "parse json error");
        }*/
    }


    /**
     * Show dialog
     */
    private static void goToDownload(Context context, String downloadUrl) {
        Intent intent = new Intent(context.getApplicationContext(), DownloadService.class);
        intent.putExtra(Constants.APK_DOWNLOAD_URL, downloadUrl);
        context.startService(intent);
    }

}
