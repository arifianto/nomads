package com.nomads.nomads.autoUpdateApk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.system.ErrnoException;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.qjcode.arm.api.Shell;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

public class ApkUtils {
    public static void installAPk(Context context, File apkFile) {
        /*Intent installAPKIntent = getApkInStallIntent(context, apkFile);
        context.startActivity(installAPKIntent);*/

        Uri uri = getApkUri(apkFile);
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setDataAndType(uri, "application/vnd.android.package-archive");
//        context.startActivity(intent);

        Log.e("cek apkFile", "apkFile " + apkFile);
        Log.e("cek apk", "Installing apk at " + uri);

        Process su = null;
        Shell shell = new Shell();

        shell.execute("am kill com.nomads.nomads");
        shell.execute("pm install -r " + apkFile);
        shell.execute("am start -n com.nomads.nomads/com.nomads.nomads.SplashScreen");

        try {
            su = Runtime.getRuntime().exec("su");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            outputStream.writeBytes("pm install -r " + apkFile + "\n");
            outputStream.flush();
            Log.e("KIL", "installAPk: INSTALL SUCCESS" );
            outputStream.writeBytes("am start -n com.nomads.nomads/com.nomads.nomads.SplashScreen \\n");
            outputStream.flush();
            Log.e("KIL", "installAPk: START SUCCESS" );

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();

//            su = Runtime.getRuntime().exec("sh su");
//            String a = "am kill com.nomads.nomads \n";
//            su.getOutputStream().write(a.getBytes());
//            String cmd = "pm install -r " + apkFile + "\n";
//            su.getOutputStream().write(cmd.getBytes());
//            String c = "am start -n com.nomads.nomads/com.nomads.nomads.SplashScreen \n";
//            su.getOutputStream().write(c.getBytes());
//            String exit = "exit\n";
//            su.getOutputStream().write(exit.getBytes());
//            su.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (su != null) {
                su.destroy();
            }
        }
    }


    public static Intent getApkInStallIntent(Context context, File apkFile) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".update.provider", apkFile);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        } else {
            Uri uri = getApkUri(apkFile);
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        }
        return intent;
    }


    private static Uri getApkUri(File apkFile) {
        Log.d(Constants.TAG, apkFile.toString());
        try {
            String[] command = {"chmod", "777", apkFile.toString()};
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
        } catch (IOException ignored) {
        }
        Uri uri = Uri.fromFile(apkFile);
        Log.d(Constants.TAG, uri.toString());
        return uri;
    }
}
