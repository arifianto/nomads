package com.nomads.nomads.autoUpdateApk;

public class Constants {

    public static final String APK_DOWNLOAD_URL = "url";
    static final String APK_VERSION_CODE = "version_code";
    static final String APK_UPDATE_CONTENT = "update_message";
    static final String DATA = "data";

    static final String TAG = "UpdateChecker";
    static final int TYPE_DIALOG = 1;
    static final String UPDATE_URL = "http://147.139.163.206:8080/update-apps/check";
}
