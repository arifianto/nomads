package com.nomads.nomads.downloadVideo;

import com.downloader.Error;
import com.downloader.Progress;

public interface DownloadVideoListener  {

    void onDownloadProgress(Progress progress);
    void onDownloadComplete();
    void onDownloadError(Error error);

}
