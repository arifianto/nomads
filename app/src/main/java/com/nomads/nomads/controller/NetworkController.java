package com.nomads.nomads.controller;

public class NetworkController {

    private final SettingsController settingsController;

    public NetworkController(SettingsController settingsController) {
        this.settingsController = settingsController;
    }

    public void putToken(String key, String value) {
        settingsController.putString(key, value);
    }

    public String getToken(String key){
        return  settingsController.getString(key);
    }

    public void putDistance(String value) {
        settingsController.putString("distance", value);
    }

    public String getDistance(){
        return  settingsController.getString("distance");
    }


}
