package com.nomads.nomads.controller;

import android.content.Context;
import android.content.SharedPreferences;

import com.nomads.nomads.BuildConfig;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class SettingsController {

    private static final String TAG = SettingsController.class.getCanonicalName();

    // filename
    private static final String FILENAME = "Nomads";

    // keys
    private static final String KEY_IP = "server_ip";
    private static final String KEY_PORT = "server_port";

    // default values

    private SharedPreferences sharedPreferences;

    public SettingsController(Context context) {
        sharedPreferences = context.getSharedPreferences(FILENAME, MODE_PRIVATE);
        if (!sharedPreferences.contains(KEY_IP) || !sharedPreferences.contains(KEY_PORT)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.apply();
        }
    }

    public void putLong(String key, long value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public long getLong(String name) {
        return sharedPreferences.getLong(name, 0L);
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    // Clear Shared Preference
    public void clear(String name) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(name);
        editor.apply();
    }



    public static String getProgressDisplayLine(long currentBytes, long totalBytes) {
        return getBytesToMBString(currentBytes) + "/" + getBytesToMBString(totalBytes);
    }
    public static String getProgressDisplayLine(long currentBytes) {
        return getBytesToMBString(currentBytes) ;
    }
    private static String getBytesToMBString(long bytes){
        return String.format(Locale.ENGLISH, "%.2fMb", bytes / (1024.00 * 1024.00));
    }
}
