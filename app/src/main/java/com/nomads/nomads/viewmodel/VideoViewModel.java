package com.nomads.nomads.viewmodel;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.nomads.nomads.model.VideoModel;
import com.nomads.nomads.network.request.CampaignEnd;
import com.nomads.nomads.network.request.CampaignPlay;
import com.nomads.nomads.network.request.CampaignStart;
import com.nomads.nomads.network.response.campaign.CampaignPlayResponse;
import com.nomads.nomads.network.response.campaign.CampaignStartResponse;
import com.nomads.nomads.network.response.login.LoginResponse;
import com.nomads.nomads.network.response.updateApp.UpdateAppResponse;
import com.nomads.nomads.network.response.video.VideoResponse;
import com.nomads.nomads.network.response.video.historydownloadvideo.HistoryDownloadVideo;
import com.nomads.nomads.orm.model.EntityHistoryDownload;
import com.nomads.nomads.orm.model.EntityPlayingHistory;
import com.nomads.nomads.orm.model.EntityVideo1;
import com.nomads.nomads.orm.model.EntityVideo2;
import com.nomads.nomads.repository.CampaignRepository;
import com.nomads.nomads.repository.UserRepository;
import com.nomads.nomads.repository.VideoRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class VideoViewModel extends ViewModel {

    @Inject
    VideoRepository videoRepository;
    @Inject
    UserRepository userRepository;

    @Inject
    CampaignRepository campaignRepository;

    @Inject
    public VideoViewModel() {

    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }


    public Flowable<UpdateAppResponse> getUpdateApp() {
        return videoRepository.getUpdateApp();
    }

    public Flowable<VideoResponse> getVideoesUsingRx(String imei) {
        return videoRepository.getPlaylistVideo(imei);
    }

    public Flowable<LoginResponse> doLogin(String usernameLogin, String passwordLogin) {
        return userRepository.doLogin(usernameLogin, passwordLogin);
    }

    public Flowable<CampaignStartResponse> postCampaignStart(String token){
        return  campaignRepository.postCampaignStart(token);
    }

    public Flowable<CampaignPlayResponse> postCampaignPlay(String imei, int campaignId, CampaignPlay campaignPlay){
        return  campaignRepository.postCampaignPlay(imei, campaignId, campaignPlay);
    }

    public void insertCampaignStart(CampaignStart campaignStart){
        campaignRepository.insertCampaignStart(campaignStart);
    }

    public void insertCampaignEnd(CampaignEnd campaignEnd){
        campaignRepository.insertCampaignEnd(campaignEnd);
    }

    public EntityVideo1 getAllVideo1(){
        return videoRepository.getAllVideo1();
    }

    public EntityVideo2 getAllVideo2(){
        return videoRepository.getAllVideo2();
    }

    public List<String> getVideoAllbyName(){
        return  videoRepository.getVideoByName();

    }public List<VideoModel> getVideoAllbySlug(String name){
        return  videoRepository.getVideoBySlug(name);
    }

    public List<String> getPlaylistVideo1() {
        return  videoRepository.getVideo1();
    }

    public List<String> getPlaylistVideo2() {
        return  videoRepository.getVideo2();
    }

    public List<String> getExpiredVideo(long date) {
        return  videoRepository.getExpiredVideo(date);
    }

    public void insertHistory (EntityPlayingHistory history){
        videoRepository.insertHistory(history);
    }

    public int getDayCampaignidByVideoName(String videoSlugName) {
        return videoRepository.getDayCampaignidByVideoName(videoSlugName);
    }

    public int getNightCampaignidByVideoName(String videoSlugName) {
        return videoRepository.getNightCampaignidByVideoName(videoSlugName);
    }

    public List<EntityPlayingHistory> getHistoryVideo() {
        return videoRepository.getHistoryVideo();
    }

    public void insertHistoryDownload(EntityHistoryDownload entityHistoryDownload) {
        videoRepository.insertHistoryDownload(entityHistoryDownload);
    }

    public void deleteFieldHistoryDownload(String videoName) {
        videoRepository.deleteFieldHistory(videoName);
    }

    public void dropTableHistoryVideo() {
        videoRepository.deleteHistoryVideo();
    }

    public String getURLVideoByNameDay(String s) {
         return videoRepository.getUrlVideoByNameDay(s);
    }

    public String getURLVideoByNameNight(String s) {
        return videoRepository.getUrlVideoByNameNight(s);
    }

    public Flowable<HistoryDownloadVideo> postHistoryDownload(String imei, int videoId) {
        return videoRepository.postHistoryDownload(imei, videoId);
    }
}
