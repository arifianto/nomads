package com.nomads.nomads.viewmodel;

import androidx.lifecycle.ViewModel;

import com.nomads.nomads.network.response.login.LoginResponse;
import com.nomads.nomads.repository.UserRepository;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class UserViewModel extends ViewModel {

    @Inject
    UserRepository userRepository;

    @Inject
    public UserViewModel() {

    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

}
