package com.nomads.nomads.network.response.video;

import com.google.gson.annotations.SerializedName;

public class Video{

	@SerializedName("path")
	private String path;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("slug")
	private String slug;

	@SerializedName("expired_date")
	private String expiredDate;

	@SerializedName("status")
	private String status;

	public void setPath(String path){
		this.path = path;
	}

	public String getPath(){
		return path;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

	public void setExpiredDate(String expiredDate){
		this.expiredDate = expiredDate;
	}

	public String getExpiredDate(){
		return expiredDate;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Video{" + 
			"path = '" + path + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",slug = '" + slug + '\'' + 
			",expired_date = '" + expiredDate + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}