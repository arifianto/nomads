package com.nomads.nomads.network.response.campaign;

import com.google.gson.annotations.SerializedName;

public class CampaignPlayResponse {

	@SerializedName("status_message")
	private String statusMessage;

	@SerializedName("status_code")
	private String statusCode;

	@SerializedName("data")
	private Object data;

	public void setStatusMessage(String statusMessage){
		this.statusMessage = statusMessage;
	}

	public String getStatusMessage(){
		return statusMessage;
	}

	public void setStatusCode(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode(){
		return statusCode;
	}

	public void setData(Object data){
		this.data = data;
	}

	public Object getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"CampaignStart{" +
			"status_message = '" + statusMessage + '\'' + 
			",status_code = '" + statusCode + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}