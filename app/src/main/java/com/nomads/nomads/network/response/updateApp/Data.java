package com.nomads.nomads.network.response.updateApp;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("version_code")
	private int versionCode;

	@SerializedName("id")
	private int id;

	@SerializedName("url")
	private String url;

	@SerializedName("update_message")
	private String updateMessage;

	public void setVersionCode(int versionCode){
		this.versionCode = versionCode;
	}

	public int getVersionCode(){
		return versionCode;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setUpdateMessage(String updateMessage){
		this.updateMessage = updateMessage;
	}

	public String getUpdateMessage(){
		return updateMessage;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"version_code = '" + versionCode + '\'' + 
			",id = '" + id + '\'' + 
			",url = '" + url + '\'' + 
			",update_message = '" + updateMessage + '\'' + 
			"}";
		}
}