package com.nomads.nomads.network.request;

import com.google.gson.annotations.SerializedName;

public class CampaignEnd {

	@SerializedName("driver_id")
	private int driverId;

	@SerializedName("device_id")
	private String deviceId;

	@SerializedName("time")
	private String time;

	@SerializedName("lang")
	private double lang;

	@SerializedName("vehicle_id")
	private int vehicleId;

	@SerializedName("long")
	private double jsonMemberLong;

	public void setDriverId(int driverId){
		this.driverId = driverId;
	}

	public int getDriverId(){
		return driverId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setLang(double lang){
		this.lang = lang;
	}

	public double getLang(){
		return lang;
	}

	public void setVehicleId(int vehicleId){
		this.vehicleId = vehicleId;
	}

	public int getVehicleId(){
		return vehicleId;
	}

	public void setJsonMemberLong(double jsonMemberLong){
		this.jsonMemberLong = jsonMemberLong;
	}

	public double getJsonMemberLong(){
		return jsonMemberLong;
	}

	@Override
 	public String toString(){
		return 
			"CampaignStartResponse{" +
			"driver_id = '" + driverId + '\'' + 
			",device_id = '" + deviceId + '\'' + 
			",time = '" + time + '\'' + 
			",lang = '" + lang + '\'' + 
			",vehicle_id = '" + vehicleId + '\'' + 
			",long = '" + jsonMemberLong + '\'' + 
			"}";
		}
}