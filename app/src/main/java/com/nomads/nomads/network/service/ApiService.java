package com.nomads.nomads.network.service;

import com.nomads.nomads.network.request.CampaignPlay;
import com.nomads.nomads.network.request.CampaignStart;
import com.nomads.nomads.network.request.CampaignEnd;
import com.nomads.nomads.network.request.UserLogin;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @GET("/versions/check")
    Call<ResponseBody> getUpdateApp();

    @GET("/campaigns/playlist")
    Call<ResponseBody> getPlaylistVideoAll(@Header("Imei") String imei);

    @POST("/login")
    Call<ResponseBody> doLogin(@Body UserLogin userLogin);

    @POST("/campaigns/1/start")
    Call<ResponseBody> postCampaignStart(@Header("Token") String token, @Body CampaignStart campaignStart);

    @POST("/campaigns/{campaignId}/play")
    Call<ResponseBody> postCampaignPlay(@Header("Imei") String imei, @Path("campaignId") int campaignId,  @Body CampaignPlay campaignPlay);

    @POST("/videos/{videoId}/download")
    Call<ResponseBody> postHistoryDownload(@Header("imei") String imei, @Path("videoId") int videoId);
}
