package com.nomads.nomads.network.response.video;

import com.google.gson.annotations.SerializedName;

public class TimeZone2Item{

	@SerializedName("end_date")
	private String endDate;

	@SerializedName("name")
	private String name;

	@SerializedName("video")
	private Video video;

	@SerializedName("campaign_id")
	private int campaignId;

	@SerializedName("start_date")
	private String startDate;

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setVideo(Video video){
		this.video = video;
	}

	public Video getVideo(){
		return video;
	}

	public void setCampaignId(int campaignId){
		this.campaignId = campaignId;
	}

	public int getCampaignId(){
		return campaignId;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}

	@Override
 	public String toString(){
		return 
			"TimeZone2Item{" + 
			"end_date = '" + endDate + '\'' + 
			",name = '" + name + '\'' + 
			",video = '" + video + '\'' + 
			",campaign_id = '" + campaignId + '\'' + 
			",start_date = '" + startDate + '\'' + 
			"}";
		}
}