package com.nomads.nomads.network.response.video;

import com.google.gson.annotations.SerializedName;

public class VideoResponse{

	@SerializedName("status_message")
	private String statusMessage;

	@SerializedName("status_code")
	private String statusCode;

	@SerializedName("data")
	private Data data;

	public void setStatusMessage(String statusMessage){
		this.statusMessage = statusMessage;
	}

	public String getStatusMessage(){
		return statusMessage;
	}

	public void setStatusCode(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode(){
		return statusCode;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"VideoResponse{" + 
			"status_message = '" + statusMessage + '\'' + 
			",status_code = '" + statusCode + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}