package com.nomads.nomads.network.request;

import com.google.gson.annotations.SerializedName;

public class UserLogin {
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String passsword;

    public UserLogin() {
    }

    public UserLogin(String username, String passsword) {
        this.username = username;
        this.passsword = passsword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasssword() {
        return passsword;
    }

    public void setPasssword(String passsword) {
        this.passsword = passsword;
    }
}
