package com.nomads.nomads.network.request;

import com.google.gson.annotations.SerializedName;

public class CampaignPlay{

	@SerializedName("start_time")
	private String startTime;

	@SerializedName("distance")
	private double distance;

	@SerializedName("end_time")
	private String endTime;

	@SerializedName("line")
	private String line;

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	public void setLine(String line){
		this.line = line;
	}

	public String getLine(){
		return line;
	}

	@Override
 	public String toString(){
		return 
			"CampaignPlay{" + 
			"start_time = '" + startTime + '\'' + 
			",distance = '" + distance + '\'' + 
			",end_time = '" + endTime + '\'' + 
			",line = '" + line + '\'' +
			"}";
		}
}