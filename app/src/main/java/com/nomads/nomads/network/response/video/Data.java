package com.nomads.nomads.network.response.video;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("time_zone_2")
	private List<TimeZone2Item> timeZone2;

	@SerializedName("time_zone_1")
	private List<TimeZone1Item> timeZone1;

	public void setTimeZone2(List<TimeZone2Item> timeZone2){
		this.timeZone2 = timeZone2;
	}

	public List<TimeZone2Item> getTimeZone2(){
		return timeZone2;
	}

	public void setTimeZone1(List<TimeZone1Item> timeZone1){
		this.timeZone1 = timeZone1;
	}

	public List<TimeZone1Item> getTimeZone1(){
		return timeZone1;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"time_zone_2 = '" + timeZone2 + '\'' + 
			",time_zone_1 = '" + timeZone1 + '\'' + 
			"}";
		}
}