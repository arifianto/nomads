package com.nomads.nomads.network.response.video.historydownloadvideo;

import com.google.gson.annotations.SerializedName;

public class HistoryDownloadVideo{

	@SerializedName("status_message")
	private String statusMessage;

	@SerializedName("status_code")
	private String statusCode;

	@SerializedName("data")
	private Object data;

	public void setStatusMessage(String statusMessage){
		this.statusMessage = statusMessage;
	}

	public String getStatusMessage(){
		return statusMessage;
	}

	public void setStatusCode(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode(){
		return statusCode;
	}

	public void setData(Object data){
		this.data = data;
	}

	public Object getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"HistoryDownloadVideo{" + 
			"status_message = '" + statusMessage + '\'' + 
			",status_code = '" + statusCode + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}