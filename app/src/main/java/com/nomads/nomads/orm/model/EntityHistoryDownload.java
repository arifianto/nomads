package com.nomads.nomads.orm.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "historydownload")
public class EntityHistoryDownload {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long id;

    @ColumnInfo
    private String videoName;

    @ColumnInfo
    private long expiredVideo;

    public EntityHistoryDownload() {
    }

    public EntityHistoryDownload(long id, String videoName, long expiredVideo) {
        this.id = id;
        this.videoName = videoName;
        this.expiredVideo = expiredVideo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public long getExpiredVideo() {
        return expiredVideo;
    }

    public void setExpiredVideo(long expiredVideo) {
        this.expiredVideo = expiredVideo;
    }
}
