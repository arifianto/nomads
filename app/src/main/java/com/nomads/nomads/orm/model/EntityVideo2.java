package com.nomads.nomads.orm.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.nomads.nomads.helper.Utils;
import com.nomads.nomads.network.response.video.Video;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "video2")
public class EntityVideo2 {

	@PrimaryKey
	@NonNull
	private int campaignId;

	@ColumnInfo
	private int id;

	@ColumnInfo
	private String path;

	@ColumnInfo
	private String name;

	@ColumnInfo
	private String slug;

	@ColumnInfo
	private long expiredDate;

	@ColumnInfo
	private String status;

	public EntityVideo2() {
	}

	public EntityVideo2(Video video, int campaignId) {
		this.id = video.getId();
		this.campaignId = campaignId;
		this.path = video.getPath();
		this.name = video.getName();
		this.slug = video.getSlug();
		this.expiredDate = Utils.parseTimetoLong(video.getExpiredDate());
		this.status = video.getStatus();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public long getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(long expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}