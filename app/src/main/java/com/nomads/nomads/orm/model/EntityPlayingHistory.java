package com.nomads.nomads.orm.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "historyVideo")
public class EntityPlayingHistory {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo
    private int campaignId;

    @ColumnInfo
    private String startTime;

    @ColumnInfo
    private double distance;

    @ColumnInfo
    private String endTime;

    @ColumnInfo
    private String singleLineLocation;

    public EntityPlayingHistory() {
    }

    public EntityPlayingHistory(int campaignId, String startTime, double distance, String endTime, String singleLineLocation) {
        this.campaignId = campaignId;
        this.startTime = startTime;
        this.distance = distance;
        this.endTime = endTime;
        this.singleLineLocation = singleLineLocation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSingleLineLocation() {
        return singleLineLocation;
    }

    public void setSingleLineLocation(String singleLineLocation) {
        this.singleLineLocation = singleLineLocation;
    }
}
