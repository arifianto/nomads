package com.nomads.nomads.orm.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.nomads.nomads.network.request.CampaignEnd;
import com.nomads.nomads.network.request.CampaignStart;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "campaign")
public class EntityCampaign{

	@PrimaryKey(autoGenerate = true)
	@NonNull
	private long id;

	@ColumnInfo
	private int driverId;

	@ColumnInfo
	private String deviceId;

	@ColumnInfo
	private String time;

	@ColumnInfo
	private double lang;

	@ColumnInfo
	private double jsonMemberLong;

	public EntityCampaign() {
	}

	public EntityCampaign(CampaignStart campaignStart) {
		this.driverId = campaignStart.getDriverId();
		this.deviceId = campaignStart.getDeviceId();
		this.time = campaignStart.getTime();
		this.lang = campaignStart.getLang();
		this.jsonMemberLong = campaignStart.getJsonMemberLong();
	}

	public EntityCampaign(CampaignEnd campaignEnd) {
		this.driverId = campaignEnd.getDriverId();
		this.deviceId = campaignEnd.getDeviceId();
		this.time = campaignEnd.getTime();
		this.lang = campaignEnd.getLang();
		this.jsonMemberLong = campaignEnd.getJsonMemberLong();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDriverId(int driverId){
		this.driverId = driverId;
	}

	public int getDriverId(){
		return driverId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setLang(double lang){
		this.lang = lang;
	}

	public double getLang(){
		return lang;
	}

	public void setJsonMemberLong(double jsonMemberLong){
		this.jsonMemberLong = jsonMemberLong;
	}

	public double getJsonMemberLong(){
		return jsonMemberLong;
	}

}