package com.nomads.nomads.orm.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.nomads.nomads.network.response.login.LoginResponse;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "user")
public class EntityUser {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long userID;

    @ColumnInfo
    private String username;

    @ColumnInfo
    private String token;

    public EntityUser() {
    }

    public EntityUser(String usernameLogin, LoginResponse loginResponse) {
        this.username = usernameLogin;
        this.token = loginResponse.getData().getToken();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
