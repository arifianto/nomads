package com.nomads.nomads.orm.dao;

import android.util.Log;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.nomads.nomads.model.VideoModel;
import com.nomads.nomads.network.response.video.Video;
import com.nomads.nomads.orm.model.EntityHistoryDownload;
import com.nomads.nomads.orm.model.EntityPlayingHistory;
import com.nomads.nomads.orm.model.EntityUser;
import com.nomads.nomads.orm.model.EntityVideo1;
import com.nomads.nomads.orm.model.EntityVideo2;

import java.util.List;

@Dao
public interface VideoDao {
    String TAG = Video.class.getCanonicalName();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPlaylist1(List<EntityVideo1> videos);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPlaylist2(List<EntityVideo2> videos);

    @Insert
    void insert(EntityVideo1 video);

    @Query("SELECT * FROM video1")
    EntityVideo1 getAllVideo1();

    @Query("SELECT * FROM video2")
    EntityVideo2 getAllVideo2();

    @Query("SELECT * FROM video1 WHERE slug LIKE :name")
    List<Video> getVideo1byName(String name);

    @Query("SELECT * FROM video2 WHERE slug LIKE :name")
    List<EntityVideo2> getVideo2byName(String name);

    @Query("SELECT slug FROM video1 UNION SELECT slug FROM video2" )
    List<String> getAllVideoName();

    @Query("SELECT * FROM video1 WHERE slug LIKE :serachname UNION SELECT * FROM video2 WHERE slug LIKE :serachname" )
    List<VideoModel> getAllVideoBySlug(String serachname);

    @Query("SELECT slug FROM video1")
    List<String> getSlugVideo1();

    @Query("SELECT slug FROM video2")
    List<String> getSlugVideo2();

    @Query("SELECT videoName FROM historydownload WHERE expiredVideo < :searchdate" )
    List<String> getExpiredVideo(long searchdate);

    // ===== History play ======
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertHistoryPlayVideo(EntityPlayingHistory history);

    @Query("SELECT * from historyVideo WHERE startTime LIKE :startTime")
    EntityPlayingHistory getHisoryPlayVideo(String startTime);

    default void insertHistoryVideo(EntityPlayingHistory entityPlayingHistory){
        EntityPlayingHistory itemFromDB = getHisoryPlayVideo(entityPlayingHistory.getStartTime());
        if (itemFromDB == null) {
            insertHistoryPlayVideo(entityPlayingHistory);
            Log.e(TAG, "insertHistoryDownload: insert History Play Success" );
        } else {
            Log.e(TAG, "insertHistoryVideo: duplicate not inserted history video");
        }
    }
    // ==========================

    @Query("SELECT campaignId FROM video1 WHERE slug LIKE :videoSlugName")
    int getDayCampaignIdByVideoName(String videoSlugName);

    @Query("SELECT campaignId FROM video2 WHERE slug LIKE :videoSlugName")
    int getNightCampaignIdByVideoName(String videoSlugName);

    @Query("SELECT * FROM historyVideo")
    List<EntityPlayingHistory> getHistoryVideo();

    // ======== History Download ===========
    @Insert
    void insertEntityHistoryDownload(EntityHistoryDownload entityHistoryDownload);

    @Query("UPDATE historydownload SET expiredVideo = :expireddate WHERE videoName LIKE :videoName")
    void updateToken(String videoName, long expireddate);

    @Query("SELECT * from historydownload WHERE videoName LIKE :videoName")
    EntityHistoryDownload getHisoryDownload(String videoName);

    @Query("DELETE FROM historydownload WHERE videoName LIKE :videoName")
    void deleteHistoryDownloadbyName(String videoName);

    default void insertHistoryDownload(EntityHistoryDownload entityHistoryDownload){
        EntityHistoryDownload itemFromDB = getHisoryDownload(entityHistoryDownload.getVideoName());
        if (itemFromDB == null) {
            insertEntityHistoryDownload(entityHistoryDownload);
            Log.e("InsertHistoryDownload", "insertOrUpdate: insert video history success" );
        } else {
            updateToken(entityHistoryDownload.getVideoName(), entityHistoryDownload.getExpiredVideo() );
            Log.e("InsertHistoryDownload", "insertOrUpdate: update video history success" );
        }
    }

    @Query("DELETE FROM historyVideo")
    void deleteHistoryVideo();

    @Query("SELECT path FROM video1 WHERE slug LIKE :s")
    String getUrlVideobyNameDay(String s);

    @Query("SELECT path FROM video2 WHERE slug LIKE :s")
    String getUrlVideobyNameNight(String s);

}
