package com.nomads.nomads.orm;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.nomads.nomads.orm.dao.CampaignDao;
import com.nomads.nomads.orm.dao.UserDao;
import com.nomads.nomads.orm.dao.VideoDao;
import com.nomads.nomads.orm.model.EntityCampaign;
import com.nomads.nomads.orm.model.EntityHistoryDownload;
import com.nomads.nomads.orm.model.EntityPlayingHistory;
import com.nomads.nomads.orm.model.EntityUser;
import com.nomads.nomads.orm.model.EntityVideo1;
import com.nomads.nomads.orm.model.EntityVideo2;

@Database(
        entities = {
                EntityVideo1.class,
                EntityVideo2.class,
                EntityUser.class,
                EntityCampaign.class,
                EntityPlayingHistory.class,
                EntityHistoryDownload.class
        }
        , version = 1
        , exportSchema = false
)
public abstract class AppDatabase extends RoomDatabase {
    public abstract VideoDao videoDao();
    public abstract UserDao userDao();
    public abstract CampaignDao campaignDao();
}
