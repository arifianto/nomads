package com.nomads.nomads.orm.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.nomads.nomads.orm.model.EntityCampaign;

@Dao
public interface CampaignDao {

    @Insert
    void insert(EntityCampaign entityCampaign);

    @Query("Select * from campaign")
    EntityCampaign getAllCampaign();

}
