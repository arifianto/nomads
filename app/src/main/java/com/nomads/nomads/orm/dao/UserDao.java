package com.nomads.nomads.orm.dao;

import android.nfc.Tag;
import android.util.Log;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.nomads.nomads.orm.model.EntityUser;

import java.util.List;

@Dao
public interface UserDao {
    public static String TAG = UserDao.class.getCanonicalName();

    @Insert
    void insertAll(List<EntityUser> entityUsers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(EntityUser entityUser);

    @Query("SELECT * from user WHERE username LIKE :username")
    EntityUser getItemByuser(String username);

    @Query("Select * from user where username = :username")
    List<EntityUser> getUser(String username);

    @Query("UPDATE user SET token = :mtoken WHERE username LIKE :username")
    void updateToken(String username, String mtoken);

    default void insertOrUpdate(EntityUser user) {
        EntityUser itemFromDB = getItemByuser(user.getUsername());
        if (itemFromDB == null) {
            insert(user);
            Log.e(TAG, "insertOrUpdate: insert user success" );
        } else {
            updateToken(user.getUsername(), user.getToken());
            Log.e(TAG, "insertOrUpdate: update user success" );
        }
    }

}
