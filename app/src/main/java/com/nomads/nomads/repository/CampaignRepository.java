package com.nomads.nomads.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.nomads.nomads.network.request.CampaignPlay;
import com.nomads.nomads.network.request.CampaignStart;
import com.nomads.nomads.network.request.CampaignEnd;
import com.nomads.nomads.network.response.campaign.CampaignPlayResponse;
import com.nomads.nomads.network.response.campaign.CampaignStartResponse;
import com.nomads.nomads.network.service.ApiService;
import com.nomads.nomads.orm.dao.CampaignDao;
import com.nomads.nomads.orm.model.EntityCampaign;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class CampaignRepository {

    private static final String TAG = CampaignRepository.class.getCanonicalName();
    private final CampaignDao campaignDao;
    private ApiService service;

    public CampaignRepository(CampaignDao campaignDao, ApiService service) {
        this.campaignDao = campaignDao;
        this.service = service;
    }


    public Flowable<CampaignStartResponse> postCampaignStart(String token) {
        return Flowable.create(emitter -> {
            CampaignStart campaignStart = new CampaignStart();
            service.postCampaignStart(token, campaignStart).enqueue(new Callback<ResponseBody>() {

                @Override
                @EverythingIsNonNull
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            String json = response.body().string();
                            Log.d(TAG, "onResponse: "+json);
                            CampaignStartResponse campaignStartResponse = new Gson().fromJson(json, CampaignStartResponse.class);
                            emitter.onNext(campaignStartResponse);
                            return;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    emitter.onError(new Throwable("Oops, sorry something any problem"));
                    return;
                }

                @Override
                @EverythingIsNonNull
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    emitter.onError(t);
                }
            });
        }, BackpressureStrategy.BUFFER);

    }

    public Flowable<CampaignPlayResponse> postCampaignPlay(String imei, int campaignId, CampaignPlay campaignPlay) {
        return Flowable.create(emitter -> {
            service.postCampaignPlay(imei, campaignId, campaignPlay).enqueue(new Callback<ResponseBody>() {

                @Override
                @EverythingIsNonNull
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            String json = response.body().string();
                            Log.d(TAG, "onResponse campaign end: "+json);
                            CampaignPlayResponse campaignPlayResponse = new Gson().fromJson(json, CampaignPlayResponse.class);
                            emitter.onNext(campaignPlayResponse);
                        }else {
                            Log.e(TAG, "onResponse: send Location " + response );
                        }
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        emitter.onError(new Throwable("Connection Error"));
                        return;
                    }
                }

                @Override
                @EverythingIsNonNull
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    emitter.onError(t);
                    return;
                }
            });
        }, BackpressureStrategy.BUFFER);
    }

    public void insertCampaignStart(CampaignStart campaignStart) {
        campaignDao.insert(new EntityCampaign(campaignStart));
    }

    public void insertCampaignEnd(CampaignEnd campaignEnd) {
        campaignDao.insert(new EntityCampaign(campaignEnd));
    }
}
