package com.nomads.nomads.repository;

import android.os.Handler;

import com.google.gson.Gson;
import com.nomads.nomads.network.request.UserLogin;
import com.nomads.nomads.network.response.login.LoginResponse;
import com.nomads.nomads.network.service.ApiService;
import com.nomads.nomads.orm.dao.UserDao;
import com.nomads.nomads.orm.model.EntityUser;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class UserRepository {

    private final UserDao userDao;
    private ApiService service;

    public UserRepository(UserDao userDao, ApiService service) {
        this.userDao = userDao;
        this.service = service;
    }

    public Flowable<LoginResponse> doLogin(String usernameLogin, String passwordLogin) {
        return Flowable.create(emitter -> {
            UserLogin userLogin = new UserLogin(usernameLogin, passwordLogin);
            service.doLogin(userLogin).enqueue(new Callback<ResponseBody>() {

                @Override
                @EverythingIsNonNull
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            String json = response.body().string();
                            LoginResponse loginResponse = new Gson().fromJson(json, LoginResponse.class);
                            emitter.onNext(loginResponse);
                            userDao.insertOrUpdate(new EntityUser(usernameLogin, loginResponse));

                            return;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    emitter.onError(new Throwable("Oops, sorry something any problem"));
                }

                @Override
                @EverythingIsNonNull
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    emitter.onError(t);
                }
            });
        }, BackpressureStrategy.BUFFER);
    }
}
