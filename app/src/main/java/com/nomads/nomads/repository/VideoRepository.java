package com.nomads.nomads.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.nomads.nomads.model.VideoModel;
import com.nomads.nomads.network.response.updateApp.UpdateAppResponse;
import com.nomads.nomads.network.response.video.Video;
import com.nomads.nomads.network.response.video.VideoResponse;
import com.nomads.nomads.network.response.video.historydownloadvideo.HistoryDownloadVideo;
import com.nomads.nomads.network.service.ApiService;
import com.nomads.nomads.orm.dao.UserDao;
import com.nomads.nomads.orm.dao.VideoDao;
import com.nomads.nomads.orm.model.EntityHistoryDownload;
import com.nomads.nomads.orm.model.EntityPlayingHistory;
import com.nomads.nomads.orm.model.EntityVideo1;
import com.nomads.nomads.orm.model.EntityVideo2;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class VideoRepository {

    private static final String TAG = VideoRepository.class.getCanonicalName();
    private final UserDao userDao;
    private VideoDao dao;
    private ApiService service;

    public VideoRepository(VideoDao dao, UserDao userDao, ApiService service) {
        this.userDao = userDao;
        this.dao = dao;
        this.service = service;
    }

    public Flowable<UpdateAppResponse> getUpdateApp() {
        return Flowable.create(emitter -> {
            Call<ResponseBody> call = service.getUpdateApp();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                @EverythingIsNonNull
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            String json = response.body().string();
                            UpdateAppResponse updateAppResponse = new Gson().fromJson(json, UpdateAppResponse.class);
                            emitter.onNext(updateAppResponse);
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    emitter.onError(new Throwable("Connection error"));
                }

                @Override
                @EverythingIsNonNull
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    emitter.onError(t);
                }
            });
        }, BackpressureStrategy.BUFFER);
    }

    public Flowable<VideoResponse> getPlaylistVideo(String imei) {
        return Flowable.create(emitter -> {
            Call<ResponseBody> call = service.getPlaylistVideoAll(imei);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                @EverythingIsNonNull
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.e(TAG, "RESPONSE GET PLAYLIST ");
                        if (response.isSuccessful() && response.body() != null) {
                            String json = response.body().string();
                            Log.e(TAG, "GET PLAYLIST " + json);
                            VideoResponse videoResponse = new Gson().fromJson(json, VideoResponse.class);

                            // define null on object timezone
                            if (videoResponse.getData().getTimeZone1() == null || videoResponse.getData().getTimeZone2() == null) {
                                // Timezone null
                                emitter.onNext(videoResponse);
                            } else {
                                List<EntityVideo1> playlistItems1 = new ArrayList<>();
                                if (videoResponse.getData().getTimeZone1().size() > 0) {
                                    for (int i = 0; i < videoResponse.getData().getTimeZone1().size(); i++) {
                                        Video playlist1 = videoResponse.getData().getTimeZone1().get(i).getVideo();
                                        playlistItems1.add(new EntityVideo1(playlist1, videoResponse.getData().getTimeZone1().get(i).getCampaignId()));
                                    }
                                    dao.insertPlaylist1(playlistItems1);
                                }
                                List<EntityVideo2> playlistItems2 = new ArrayList<>();
                                if (videoResponse.getData().getTimeZone2().size() > 0) {
                                    for (int i = 0; i < videoResponse.getData().getTimeZone2().size(); i++) {
                                        Video playlist2 = videoResponse.getData().getTimeZone2().get(i).getVideo();
                                        playlistItems2.add(new EntityVideo2(playlist2, videoResponse.getData().getTimeZone2().get(i).getCampaignId()));
                                    }
                                    dao.insertPlaylist2(playlistItems2);
                                }
                                Log.e(TAG, "onResponse: insert video to playlist success");
                                emitter.onNext(videoResponse);
                            }
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        emitter.onError(new Throwable("Maaf, terjadi kesalahan"));
                        return;
                    }
                }

                @Override
                @EverythingIsNonNull
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    emitter.onError(t);
                    return;
                }
            });
        }, BackpressureStrategy.BUFFER);
    }


    public void insertHistory(EntityPlayingHistory history) {
        dao.insertHistoryVideo(history);
    }

    public EntityVideo1 getAllVideo1() {
        return dao.getAllVideo1();
    }

    public EntityVideo2 getAllVideo2() {
        return dao.getAllVideo2();
    }

    public List<String> getVideoByName() {
        return dao.getAllVideoName();
    }

    public List<VideoModel> getVideoBySlug(String searchName) {
        return dao.getAllVideoBySlug(searchName);
    }

    public List<String> getVideo1() {
        return dao.getSlugVideo1();
    }

    public List<String> getVideo2() {
        return dao.getSlugVideo2();
    }

    public List<String> getExpiredVideo(long date) {
        return dao.getExpiredVideo(date);
    }

    public int getDayCampaignidByVideoName(String videoSlugName) {
        return dao.getDayCampaignIdByVideoName(videoSlugName);
    }

    public int getNightCampaignidByVideoName(String videoSlugName) {
        return dao.getNightCampaignIdByVideoName(videoSlugName);
    }

    public List<EntityPlayingHistory> getHistoryVideo() {
        return dao.getHistoryVideo();
    }

    public void insertHistoryDownload(EntityHistoryDownload entityHistoryDownload) {
        dao.insertHistoryDownload(entityHistoryDownload);
    }

    public void deleteFieldHistory(String videoName) {
        dao.deleteHistoryDownloadbyName(videoName);
    }

    public void deleteHistoryVideo() {
        dao.deleteHistoryVideo();
    }

    public String getUrlVideoByNameDay(String s) {
        return dao.getUrlVideobyNameDay(s);
    }

    public String getUrlVideoByNameNight(String s) {
        return dao.getUrlVideobyNameNight(s);
    }

    public Flowable<HistoryDownloadVideo> postHistoryDownload(String imei, int videoId) {
        return Flowable.create(emitter -> {
            Call<ResponseBody> call = service.postHistoryDownload(imei, videoId);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                @EverythingIsNonNull
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        HistoryDownloadVideo historyDownloadVideo = new Gson().fromJson(response.body().toString(), HistoryDownloadVideo.class);
                        emitter.onNext(historyDownloadVideo);
                    }catch (Exception e){
                        e.printStackTrace();
                        emitter.onError(e);
                    }
                }

                @Override
                @EverythingIsNonNull
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    emitter.onError(t);
                }
            });
        }, BackpressureStrategy.BUFFER);
    }

}
