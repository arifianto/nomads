package com.nomads.nomads.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.nomads.nomads.downloadVideo.DownloadVideoListener;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {


    private static final String TAG = Utils.class.getCanonicalName();

    // download
    public static int downloadVideo(String url, String fileName, String path, DownloadVideoListener listener) {
        return PRDownloader.download(url, path, fileName + ".mp4")
                .build()
                .setOnStartOrResumeListener(() -> Log.e(TAG, "onStartOrResume: "))
                .setOnPauseListener(() -> Log.e(TAG, "onPause: "))
                .setOnCancelListener(() -> Log.e(TAG, "onCancel: "))
                .setOnProgressListener(progress -> {
                    Log.e(TAG, "onDownloadProgress: ");
                    listener.onDownloadProgress(progress);
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        listener.onDownloadComplete();
                    }

                    @Override
                    public void onError(Error error) {
                        Log.e(TAG, "onDownloadError: ");
                        listener.onDownloadError(error);
                    }
                });
    }

    public static int getVersionCode(Context mContext) {
        if (mContext != null) {
            try {
                return mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException ignored) {
            }
        }
        return 0;
    }

    public static boolean isConnected(Context ctx) {
        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean connected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return connected;
    }

    public static String getTime() {
        Calendar c = Calendar.getInstance();
        Locale locale = new Locale("in", "ID");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", locale);
        String time = sdf.format(c.getTime());
        Log.d("Date", "DATE : " + time);
        return time;
    }

    public static long parseTimetoLong(String dateString) {
        long formatedTime = 0;
        try {
            Locale locale = new Locale("in", "ID");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", locale);
            Date date = sdf.parse(dateString);
            formatedTime = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatedTime;
    }

    public static String parseDateTime(String date) {
        try {
            Locale locale = new Locale("in", "ID");
            Date newDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", locale).parse(date);
            return new SimpleDateFormat("dd MMMM yyyy", locale).format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    @SuppressLint("HardwareIds")
    public static String getIMEI(Context ctx) throws NullPointerException {
        TelephonyManager telephonyMgr = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String meiNumber = "";
        if (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (telephonyMgr.getImei() != null) {
                    meiNumber = telephonyMgr.getImei();
                }
            } else {
                if (telephonyMgr.getDeviceId() != null) {
                    meiNumber = telephonyMgr.getDeviceId();
                }
            }
        }
        if (meiNumber == null) {
            meiNumber = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        Log.e(TAG, "getIMEI: "+ meiNumber );
        return meiNumber;
    }

    public static boolean checkDayTime() {

        Calendar firstLimit = Calendar.getInstance();
        firstLimit.set(Calendar.HOUR, 6);
        firstLimit.set(Calendar.MINUTE, 00);
        firstLimit.set(Calendar.SECOND, 00);
        firstLimit.set(Calendar.AM_PM, Calendar.AM);

        Calendar secondLimit = Calendar.getInstance();
        secondLimit.set(Calendar.HOUR, 2);
        secondLimit.set(Calendar.MINUTE, 00);
        secondLimit.set(Calendar.SECOND, 00);
        secondLimit.set(Calendar.AM_PM, Calendar.PM);

        long startHourMilli = firstLimit.getTimeInMillis();
        long endHourMilli = secondLimit.getTimeInMillis();
        long currentMilli = Calendar.getInstance().getTimeInMillis();
        return currentMilli >= startHourMilli && currentMilli <= endHourMilli;

    }

    public static String toDecimal(double d) {
        DecimalFormat form = new DecimalFormat("0.00");
        String decimal = (form.format(d));
        return decimal;
    }


}
