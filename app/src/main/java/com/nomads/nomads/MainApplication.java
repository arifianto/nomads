package com.nomads.nomads;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.nomads.nomads.autoStartUp.NomadsReceiver;
import com.nomads.nomads.injection.AppComponent;
import com.nomads.nomads.injection.AppModule;
import com.nomads.nomads.injection.ControllerModule;
import com.nomads.nomads.injection.DaggerAppComponent;
import com.nomads.nomads.injection.NetworkModule;
import com.nomads.nomads.injection.RoomModule;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

public class MainApplication extends Application {

    private static final String TAG = MainApplication.class.getCanonicalName();
    protected AppComponent component;
    private static MainApplication mInstance;

    public AppComponent getComponent() {
        return component;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Sentry.init("https://a90adc1a53cd4ce7abc0d59a73bfcf13@sentry.io/2285502", new AndroidSentryClientFactory(this));
        component = initDagger();
        component.inject(this);
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);
    }
    protected AppComponent initDagger() {
        return DaggerAppComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this))
                .roomModule(new RoomModule(this))
                .networkModule(new NetworkModule(this))
                .controllerModule(new ControllerModule(this))
                .build();
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public static synchronized MainApplication getInstance() {
        return mInstance;
    }

}
