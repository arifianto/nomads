package com.nomads.nomads;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import com.downloader.Error;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.nomads.nomads.controller.NetworkController;
import com.nomads.nomads.downloadVideo.DownloadVideoListener;
import com.nomads.nomads.helper.MyExceptionHandler;
import com.nomads.nomads.helper.Stopwatch;
import com.nomads.nomads.helper.Utils;
import com.nomads.nomads.network.request.CampaignPlay;
import com.nomads.nomads.network.response.campaign.CampaignPlayResponse;
import com.nomads.nomads.orm.model.EntityPlayingHistory;
import com.nomads.nomads.viewmodel.VideoViewModel;
import com.nomads.nomads.viewmodel.ViewModelFactory;

import org.reactivestreams.Subscription;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.FlowableSubscriber;
import io.reactivex.schedulers.Schedulers;
import lemma.lemmavideosdk.common.LemmaSDK;
import lemma.lemmavideosdk.vast.listeners.AdManagerCallback;
import lemma.lemmavideosdk.vast.manager.LMAdRequest;
import lemma.lemmavideosdk.vast.manager.LMConfig;
import lemma.lemmavideosdk.vast.manager.LMSharedVideoManager;
import lemma.lemmavideosdk.vast.manager.LMVideoAdManager;

public class MainActivity extends AppCompatActivity implements
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener, DownloadVideoListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @Inject
    ViewModelFactory viewModelFactory;

    @Inject
    NetworkController networkController;

    private VideoViewModel videoViewModel;
    private List<Subscription> sub = new ArrayList<>();
    private static final String TAG = "MainActivity";
    private VideoView videoView = null;
    private String[] videoArray = {"v_121", "v122"};
    private String path = "";
    private int videoSquence = 0;
    private Uri videoUri;
    private List<String> dayPlaylist = new ArrayList<String>();
    private List<String> nightPlaylist = new ArrayList<String>();
    private final int START = 0;
    private final int END = 1;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private int campaignId = 0;

    // location
    private Location location;
    private TextView locationTv, tvTimer;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    // lists for permissions
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    // timer
    final int MSG_START_TIMER = 0;
    final int MSG_STOP_TIMER = 1;
    final int MSG_UPDATE_TIMER = 2;

    Stopwatch timer = new Stopwatch();
    final int REFRESH_RATE = 100;

    // lemma
    private int lemmaCampaignId = 54;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MainApplication app = (MainApplication) getApplication();
        app.getComponent().inject(this);
        videoViewModel = new ViewModelProvider(this, viewModelFactory).get(VideoViewModel.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        // set Shared Preference
        sharedPreferences = getApplicationContext().getSharedPreferences("Nomads", 0); // 0 - for private mode

        // init component
        initComponent();

        // lemma
        loadAd();
    }

    private void initComponent() {
        locationTv = findViewById(R.id.tv_location);
        videoView = findViewById(R.id.video_view);
//        videoView.setZOrderOnTop(true);
        videoView.setBackgroundColor(Color.TRANSPARENT);
        videoView.setOnCompletionListener(this);
        videoView.setOnErrorListener(this);
        initLocation();
        initstopwatch();

        // set path of video content
        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES) + "";

        //set playlist
        dayPlaylist = videoViewModel.getPlaylistVideo1();
        nightPlaylist = videoViewModel.getPlaylistVideo2();
        resetDistance();

        // get state
        if (Utils.checkDayTime()) {
            getState(Utils.checkDayTime());
        } else {
            getState(Utils.checkDayTime());
        }
        // play playlist
        playVideo(videoSquence);
    }

    // stopwatch
    private void initstopwatch() {
        tvTimer = findViewById(R.id.tv_timer);
        mHandler.sendEmptyMessage(MSG_START_TIMER);
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_START_TIMER:
                    timer.start(); //start timer
                    mHandler.sendEmptyMessage(MSG_UPDATE_TIMER);
                    break;

                case MSG_UPDATE_TIMER:
                    tvTimer.setText("" + timer.getElapsedTime());
                    mHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIMER, REFRESH_RATE); //text view is updated every second,
                    break;                                  //though the timer is still running
                case MSG_STOP_TIMER:
                    mHandler.removeMessages(MSG_UPDATE_TIMER); // no more updates.
                    timer.stop();//stop timer
                    tvTimer.setText("Deteksi " + timer.getElapsedTimeSecs() + " detik");
                    break;

                default:
                    break;
            }
        }
    };


    private void initLocation() {
        // we add permissions we need to request location of the users
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(
                        new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(this).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
    }

    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
            locationTv.setText("You need to install Google Play Services to use the App properly");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (googleApiClient != null && googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {
            mHandler.sendEmptyMessage(MSG_STOP_TIMER);
            locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
        }

        startLocationUpdates();
    }

    private void startLocationUpdates() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            String longitude = location.getLongitude() + " ";
            String latitude = location.getLatitude() + "";
            if (firstTime) {
                distance = 0;
                arrayLocation.add(longitude + latitude);
                previousLoc = location;
                firstTime = false;
            } else {
                distance += previousLoc.distanceTo(location);
                arrayLocation.add(longitude + latitude);
                Log.e(TAG, "distance " + distance);
                previousLoc = location;
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(MainActivity.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                } else {
                    if (googleApiClient != null) {
                        googleApiClient.connect();
                    }
                }

                break;
        }
    }

    double distance = 0;
    Location previousLoc = null;
    public boolean firstTime = true;

    public double getDistance() {
        Log.e(TAG, "distance sum " + distance);
        return distance;
    }

    public String getSingleLineLocation() {
        String formatedString = "";
        if (arrayLocation.size() == 1) {
            arrayLocation.add(arrayLocation.get(0));
        }
        for (int i = 0; i < arrayLocation.size(); i++) {
            formatedString = formatedString + arrayLocation.get(i);
            if (i != arrayLocation.size() - 1) {
                formatedString = formatedString + ",";
            }
        }

        return formatedString;
    }

    List<String> arrayLocation = new ArrayList<>();

    public void resetDistance() {
        distance = 0;
    }

    public void resetArrayLocation() {
        if (arrayLocation.size() > 0) {
            String tmpArrayLocation = arrayLocation.get(arrayLocation.size() - 1);
            arrayLocation.clear();
            arrayLocation.add(tmpArrayLocation);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sub.size() > 0) {
            for (Subscription s : sub) {
                s.cancel();
            }
        }
    }


    // play video
    private void playVideo(int sequence) throws NullPointerException {

        // cek hour
        if (Utils.checkDayTime()) { // play dayplaylist
            setState(true, sequence); // true for daytime
            if (dayPlaylist.size() > 0) {
                campaignId = videoViewModel.getDayCampaignidByVideoName(dayPlaylist.get(sequence));
                postLocation(START, campaignId);
                Log.e(TAG, "day campaignid: " + campaignId);
                videoUri = Uri.parse(path + "/" + dayPlaylist.get(sequence) + ".mp4");
                Log.e(TAG, "playVideo: " + videoUri);
                setVideoView(videoUri);
            } else {
                noPlaylist();
            }
        } else { // play night playlist
            setState(false, sequence); // false for night time
            if (nightPlaylist.size() > 0) {
                campaignId = videoViewModel.getNightCampaignidByVideoName(nightPlaylist.get(sequence));
                postLocation(START, campaignId);
                Log.e(TAG, "night campaignid: " + campaignId);
                videoUri = Uri.parse(path + "/" + nightPlaylist.get(sequence) + ".mp4");
                Log.e(TAG, "playVideo: " + videoUri);
                setVideoView(videoUri);
            } else {
                noPlaylist();
            }
        }
    }

    private void noPlaylist() {
        startActivity(new Intent(MainActivity.this, SplashScreen.class));
    }

    private void setVideoView(Uri videoUri) {
        videoView.setVideoURI(videoUri);
        videoView.start();
//        videoView.requestFocus();
//        videoView.setZOrderOnTop(true);
    }

    String startTime = "";

    private void postLocation(int startEnd, int campaignId) throws NullPointerException {
        String endTime = "";
        String singleLineLocation = "";
        double distance = 0;
        String imei = Utils.getIMEI(this);
        Log.e(TAG, "postLocation: IMEI" + imei);
        // post location to API
        if (Utils.isConnected(MainActivity.this)) {
            // option post on start video or end video
            if (startEnd == START) {
                startTime = Utils.getTime() + "Z";
            } else if (startEnd == END) {
                endTime = Utils.getTime() + "Z";
                singleLineLocation = getSingleLineLocation();
                resetArrayLocation();
                distance = getDistance();
                String strFormatedDistance = Utils.toDecimal(distance);
                Log.e(TAG, "postLocation: time " + startTime + " " + endTime);
                Log.e(TAG, "postLocation: distance " + strFormatedDistance);
                Log.e(TAG, "postLocation: location " + singleLineLocation);

                // post to server
                // cek db for data offline
                List<EntityPlayingHistory> listHistoryVideo = videoViewModel.getHistoryVideo();
                if (listHistoryVideo.size() > 0) {
                    // send data offline history video
                    for (int i = 0; i < listHistoryVideo.size(); i++) {
                        CampaignPlay campaignPlay = new CampaignPlay();
                        campaignPlay.setStartTime(listHistoryVideo.get(i).getStartTime());
                        campaignPlay.setEndTime(listHistoryVideo.get(i).getEndTime());
                        campaignPlay.setLine(listHistoryVideo.get(i).getSingleLineLocation());
                        campaignPlay.setDistance(listHistoryVideo.get(i).getDistance());
                        sendLocationTOServerOfflineData(listHistoryVideo.get(i).getCampaignId(), campaignPlay);
                    }
                    videoViewModel.dropTableHistoryVideo();
                }
                // send data online history video
                CampaignPlay campaignPlay = new CampaignPlay();
                campaignPlay.setStartTime(startTime);
                campaignPlay.setEndTime(endTime);
                campaignPlay.setLine(singleLineLocation);
                campaignPlay.setDistance(distance);
                sendLocationTOServer(campaignId, campaignPlay);
                resetDistance();
            }
        } else {
            // no connection
            // write to local DB
            if (startEnd == START) {
                startTime = Utils.getTime() + "Z";
            } else {
                endTime = Utils.getTime() + "Z";
                singleLineLocation = getSingleLineLocation();
                distance = getDistance();
                String strFormatedDistance = Utils.toDecimal(distance);
                Log.e(TAG, "postLocation: time " + startTime + " " + endTime);
                Log.e(TAG, "postLocation: distance " + strFormatedDistance);
                Log.e(TAG, "postLocation: location " + singleLineLocation);

                EntityPlayingHistory entityPlayingHistory = new EntityPlayingHistory();
                entityPlayingHistory.setCampaignId(campaignId);
                entityPlayingHistory.setStartTime(startTime);
                entityPlayingHistory.setEndTime(endTime);
                entityPlayingHistory.setSingleLineLocation(singleLineLocation);
                entityPlayingHistory.setDistance(distance);
                videoViewModel.insertHistory(entityPlayingHistory);
            }
            resetDistance();
        }
    }

    private void sendLocationTOServer(int campaignId, CampaignPlay campaignPlay) {
        Log.e(TAG, "sendLocationTOServer: " + campaignPlay.toString());
        Log.e(TAG, "sendLocationTOServer: " + campaignId);
        videoViewModel.postCampaignPlay(Utils.getIMEI(MainActivity.this), campaignId, campaignPlay)
                .subscribeOn(Schedulers.io())
                .subscribe(new FlowableSubscriber<CampaignPlayResponse>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        sub.add(s);
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(CampaignPlayResponse campaignPlayResponse) {
                        Log.e(TAG, "onNext Response send loc: on " + campaignPlayResponse);

                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e(TAG, "onError: send loc on " + t.getMessage());
                        Log.e(TAG, "onError: JANCOOK " + campaignPlay.getStartTime() + " End time JANCOOK " + campaignPlay.getEndTime());
                        if (!campaignPlay.getLine().isEmpty()) {
                            EntityPlayingHistory entityPlayingHistory = new EntityPlayingHistory();
                            entityPlayingHistory.setCampaignId(campaignId);
                            entityPlayingHistory.setStartTime(campaignPlay.getStartTime());
                            entityPlayingHistory.setEndTime(campaignPlay.getEndTime());
                            entityPlayingHistory.setSingleLineLocation(campaignPlay.getLine());
                            entityPlayingHistory.setDistance(campaignPlay.getDistance());
                            videoViewModel.insertHistory(entityPlayingHistory);
                        }
                    }

                    @Override
                    public void onComplete() {
                        Log.e(TAG, "onComplete:  on");
                    }
                });
    }

    private void sendLocationTOServerOfflineData(int campaignId, CampaignPlay campaignPlay) {
        Log.e(TAG, "sendLocationTOServer: off " + campaignPlay.toString());
        Log.e(TAG, "sendLocationTOServer: off " + campaignId);
        videoViewModel.postCampaignPlay(Utils.getIMEI(MainActivity.this), campaignId, campaignPlay)
                .subscribeOn(Schedulers.io())
                .subscribe(new FlowableSubscriber<CampaignPlayResponse>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        sub.add(s);
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(CampaignPlayResponse campaignPlayResponse) {
                        Log.e(TAG, "onNext Response send loc: ofline " + campaignPlayResponse);
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e(TAG, "onError: send loc offline " + t.getMessage());
                        if (!campaignPlay.getLine().isEmpty()) {
                            EntityPlayingHistory entityPlayingHistory = new EntityPlayingHistory();
                            entityPlayingHistory.setCampaignId(campaignId);
                            entityPlayingHistory.setStartTime(campaignPlay.getStartTime());
                            entityPlayingHistory.setEndTime(campaignPlay.getEndTime());
                            entityPlayingHistory.setSingleLineLocation(campaignPlay.getLine());
                            entityPlayingHistory.setDistance(campaignPlay.getDistance());
                            videoViewModel.insertHistory(entityPlayingHistory);
                        }
                    }

                    @Override
                    public void onComplete() {
                        Log.e(TAG, "onComplete: send loc off ");
                    }
                });
    }

    // video end play
    @Override
    public void onCompletion(MediaPlayer mp) {
        // day
        postLocation(END, campaignId);
        if (Utils.checkDayTime()) {
            if (videoSquence >= dayPlaylist.size() - 1) {
                playLemma();
//                videoSquence = 0;
//                playVideo(videoSquence);
            } else {
                videoSquence++;
                playVideo(videoSquence);
            }
        } else {
            // night
            if (videoSquence >= nightPlaylist.size() - 1) {
                playLemma();
//                videoSquence = 0;
//                playVideo(videoSquence);
            } else {
                videoSquence++;
                playVideo(videoSquence);
            }

        }

    }

    private void continueNomadsVideo() {
        FrameLayout fl = findViewById(R.id.ad_linear_container);
        fl.setVisibility(View.INVISIBLE);
        videoView.setVisibility(View.VISIBLE);
        videoSquence = 0;
        playVideo(videoSquence);
        loadAd();
    }

    private void playLemma() {
        // test lemma ads
        videoView.setVisibility(View.INVISIBLE);
        FrameLayout fl = findViewById(R.id.ad_linear_container);
        fl.setVisibility(View.VISIBLE);
        renderAd(LMSharedVideoManager.getInstance());

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e(TAG, "onError: Error video");
        postLocation(END, campaignId);
        // day
        if (Utils.checkDayTime()) {
            deleteFileCorupt(true);
            if (videoSquence >= dayPlaylist.size() - 1) {
                videoSquence = 0;
            } else {
                videoSquence++;
            }
        } else {
            // night
            deleteFileCorupt(false);
            if (videoSquence >= nightPlaylist.size() - 1) {
                videoSquence = 0;
            } else {
                videoSquence++;
            }
        }
        playVideo(videoSquence);
        return true;
    }

    private void setState(boolean dayTime, int state) {
        editor = sharedPreferences.edit();
        if (dayTime) { // day
            editor.putInt("DAYSTATE", state);
            editor.commit();
        } else { // night
            editor.putInt("NIGHTSTATE", state);
            editor.commit();
        }
    }

    private void getState(boolean dayTime) {
        if (dayTime) {
            int tmpDaySquence = sharedPreferences.getInt("DAYSTATE", 0);
            if (videoSquence >= dayPlaylist.size() - 1) {
                videoSquence = 0;
            } else {
                videoSquence = tmpDaySquence;
            }
        } else {
            int tmpNightSquence = sharedPreferences.getInt("NIGHTSTATE", 0);
            if (videoSquence >= nightPlaylist.size() - 1) {
                videoSquence = 0;
            } else {
                videoSquence = tmpNightSquence;
            }
        }
    }

    private void deleteFileCorupt(boolean dayTime) {
        File file;
        if (dayTime) { // day
            file = new File(path + dayPlaylist.get(videoSquence) + ".mp4");
        } else { // night
            file = new File(path + nightPlaylist.get(videoSquence) + ".mp4");
        }
        if (file.exists()) {
            String deleteCmd = "rm -r " + file;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        downloadVideoCoruptInBackground();
    }

    private int downloadId;

    private void downloadVideoCoruptInBackground() {
        if (Utils.checkDayTime()) {
            if (dayPlaylist.size() > 0) {
                String url = videoViewModel.getURLVideoByNameDay(dayPlaylist.get(videoSquence));
                downloadId = Utils.downloadVideo(url, dayPlaylist.get(videoSquence), path, this);
            }
        } else {
            if (nightPlaylist.size() > 0) {
                String url = videoViewModel.getURLVideoByNameNight(nightPlaylist.get(videoSquence));
                downloadId = Utils.downloadVideo(url, nightPlaylist.get(videoSquence), path, this);
            }
        }
    }

    @Override
    public void onDownloadProgress(Progress progress) {

    }

    @Override
    public void onDownloadComplete() {

    }

    @Override
    public void onDownloadError(Error error) {
        PRDownloader.resume(downloadId);
    }


    // Lemma Video
    private void loadAd() {
        LemmaSDK.init(this, false);
        prefetchAd();
    }

    private void prefetchAd() {

        final LMSharedVideoManager sharedVideoManager = LMSharedVideoManager.getInstance();

        //Set publisher ID and Ad-Unit id
        LMAdRequest adRequest = new LMAdRequest("248", "7277");

        //To set the Server end Point
        adRequest.setAdServerBaseURL("http://ads.lemmatechnologies.com/lemma/servad");
        LMConfig config = new LMConfig();


        //To support the offline it will keep play last downloaded content if there is no internet
        config.setPlayLastSavedLoop(false);
        config.setExecuteImpressionInWebContainer(true);

        //it will delete the content which is not in use from longer time
        config.setDeleteCacheContinuously(false);

        //it will try to retry the content from server if there is no-content available
        sharedVideoManager.setRetryCount(2);

        //it will play the current loop and download the next loop in background default value is false
        sharedVideoManager.setPrefetchNextLoop(false);

        sharedVideoManager.prepare(this, adRequest, config);
        sharedVideoManager.prefetch(new LMSharedVideoManager.LMSharedVideoManagerPrefetchCallback() {
            @Override
            public void onSuccess() {
                Log.e(TAG, "onSuccess: download ads sukses");
            }

            @Override
            public void onFailure() {
                // Called after retry count exhausts
                Log.d("No-Content", "Ads No-Content");
            }
        });
    }

    // renderAd(LMSharedVideoManager.getInstance());
    private void renderAd(final LMSharedVideoManager sharedVideoManager) {

        sharedVideoManager.renderAdInView((FrameLayout) findViewById(R.id.ad_linear_container), new AdManagerCallback() {
            @Override
            public void onAdError(LMVideoAdManager adManager, java.lang.Error error) {

            }

            @Override
            public void onAdEvent(AD_EVENT event) {
                Log.e(TAG, "onAdEvent: " + event.name());
                Log.e(TAG, "1 current loop state: " + sharedVideoManager.getCurrentAdLoopStat());
                Log.e(TAG, "1: Count Loop " + sharedVideoManager.getCurrentAdLoopStat().getCurrentAdLoopLength());
                Log.e(TAG, "1: index " + sharedVideoManager.getCurrentAdLoopStat().getCurrentAdIndex());
                int i = sharedVideoManager.getCurrentAdLoopStat().getCurrentAdIndex();
                //event
                switch (event) {
                    case AD_LOADED:
                    case AD_COMPLETED:
                        if (Utils.checkDayTime()) {
                            postLemaToServer();
                            if (i >= (10 - dayPlaylist.size())) {
                                sharedVideoManager.destroySharedInstance();
                                continueNomadsVideo();
                            }
                        } else {
                            postLemaToServer();
                            if (i >= (10 - nightPlaylist.size())) {
                                sharedVideoManager.destroySharedInstance();
                                continueNomadsVideo();
                            }
                        }
                        break;
                    case AD_LOOP_COMPLETED:
                        Log.d("AD_LOOP_COMPLETED", "AD_LOOP_COMPLETED");
                        sharedVideoManager.destroySharedInstance();
                        continueNomadsVideo();
                        break;
                }
            }
        });

    }

    private void postLemaToServer() {
        Log.e(TAG, "LEMMA");
        CampaignPlay campaignPlay = new CampaignPlay();
        campaignPlay.setStartTime(Utils.getTime() + "Z");
        campaignPlay.setEndTime(Utils.getTime() + "Z");
        campaignPlay.setDistance(getDistance());
        campaignPlay.setLine(getSingleLineLocation());
        Log.e(TAG, "LEMMA " + campaignPlay.toString());
        resetDistance();
        resetArrayLocation();
        videoViewModel.postCampaignPlay(Utils.getIMEI(MainActivity.this), lemmaCampaignId, campaignPlay)
                .subscribeOn(Schedulers.io())
                .subscribe(new FlowableSubscriber<CampaignPlayResponse>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        sub.add(s);
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(CampaignPlayResponse campaignPlayResponse) {
                        Log.e(TAG, "LEMMA POST SUCCESS" + campaignPlayResponse);

                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e(TAG, "LEMMA POST GAGAL" + t.getMessage());

                    }

                    @Override
                    public void onComplete() {
                        Log.e(TAG, "onComplete: send loc off ");
                    }
                });
    }

}
